﻿using System;
using FizzbuzzEvaluator;

namespace FizzbuzzEvaluatorConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter range (ex. 1-20):");
            var input = Console.ReadLine();

            var inputValidator = new InputValidator();
            if (!inputValidator.IsValid(input))
            {
                Console.WriteLine("Input is invalid");
            }else
            {
                var parser = new Parser();
                var numbers = parser.Parse(input);

                var report = new Report();
                var evaluator = new Evaluator(report);
                string result = evaluator.Evaluate(numbers);
                Console.WriteLine(result);
                Console.WriteLine(report.Generate());
            }

        }
    }
}
