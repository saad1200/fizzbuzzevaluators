﻿using System.Collections.Generic;
using System.Linq;
using FizzbuzzEvaluator;
using NUnit.Framework;

namespace FizzbuzzEvaluatorTest
{
    [TestFixture]
    public class ReportTest
    {
        [Test]
        public void Should_generate_report_showing_how_many_times_keyword_was_outputed()
        {
            //arrange
            var numbers = Enumerable.Range(1, 20);
            var report = new Report();
            var evaluator = new Evaluator(report);

            //assert
            evaluator.Evaluate(numbers);
            Assert.AreEqual("an integer: 10\r\nlucky: 2\r\nbuzz: 3\r\nfizz: 4\r\nfizzbuzz: 1\r\n", report.Generate());
        }

        [Test]
        public void Should_generate_report_showing_how_many_times_keyword_was_outputed_when_input_is_empty()
        {
            //arrange
            IEnumerable<int> numbers = new List<int>();
            var report = new Report();
            var evaluator = new Evaluator(report);

            //assert
            evaluator.Evaluate(numbers);
            Assert.AreEqual(string.Empty, report.Generate());
        }
    }
}
