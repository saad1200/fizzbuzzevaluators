﻿using System.Collections.Generic;
using FizzbuzzEvaluator;
using NUnit.Framework;

namespace FizzbuzzEvaluatorTest
{
    [TestFixture]
    public class ParserTest
    {
        [Test]
        public void Should_raturn_parsed_values()
        {
            //arrange
            const string input = "4-7";
            var parser = new Parser();

            //action
            IEnumerable<int> range = parser.Parse(input);

            //assert
            Assert.AreEqual("4,5,6,7", string.Join(",", range));
        }

        [Test]
        public void Should_raturn_parsed_values_when_start_number_is_greater_than_end_number()
        {
            //arrange
            const string input = "7-4";
            var parser = new Parser();

            //action
            IEnumerable<int> range = parser.Parse(input);

            //assert
            Assert.AreEqual("4,5,6,7", string.Join(",", range));
        }

        [Test]
        public void Should_raturn_parsed_values_when_start_end_numbers_are_equal()
        {
            //arrange
            const string input = "7-7";
            var parser = new Parser();

            //action
            IEnumerable<int> range = parser.Parse(input);

            //assert
            Assert.AreEqual("7", string.Join(",", range));
        }
    }
}
