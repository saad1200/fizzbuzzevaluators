﻿using System.Collections.Generic;
using System.Linq;

namespace FizzbuzzEvaluator
{
    public class Parser
    {
        public IEnumerable<int> Parse(string input)
        {
            string[] parts = input.Split('-');
            int first = int.Parse(parts[0]);
            int second = int.Parse(parts[1]);

            if(first > second)
            {
                return Enumerable.Range(second, first - second + 1);
            }
            return Enumerable.Range(first, second - first + 1);
        }
    }
}
