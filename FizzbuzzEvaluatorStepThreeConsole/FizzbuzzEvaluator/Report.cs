using System;
using System.Collections.Generic;
using System.Text;

namespace FizzbuzzEvaluator
{
    public class Report
    {
        readonly IDictionary<string, int> keywordsCounter = new Dictionary<string, int>();

        public void Add(string keyword)
        {
            if (isNumber(keyword))
                keyword = "an integer";

            if(!keywordsCounter.ContainsKey(keyword))
                keywordsCounter.Add(keyword, 0);

            keywordsCounter[keyword]++;
        }

        public string Generate()
        {
            var stringBuilder = new StringBuilder();
            foreach (var counterEntree in keywordsCounter)
            {
                stringBuilder.Append(counterEntree.Key + ": " + counterEntree.Value).Append(Environment.NewLine);
            }
            return stringBuilder.ToString();
        }

        private bool isNumber(string keyword)
        {
            int number;
            return int.TryParse(keyword, out number);
        }
    }
}