﻿using System.Linq;
using FizzbuzzEvaluator;
using NUnit.Framework;

namespace FizzbuzzEvaluatorTest
{
    [TestFixture]
    public class EvaluatorTest
    {
        [Test]
        public void Should_return_expexted_result_when_range_does_not_have_multiples_by_3_5_and_15()
        {
            //arrange
            var numbers = Enumerable.Range(1, 2);
            var evaluator = new Evaluator();

            //arrange
            string actual = evaluator.Evaluate(numbers);

            //assert
            const string expected = "1 2";
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Should_return_expexted_result_when_range_contains_a_number_that_is_multiples_by_3()
        {
            //arrange
            var numbers = Enumerable.Range(1, 4);
            var evaluator = new Evaluator();

            //arrange
            string actual = evaluator.Evaluate(numbers);

            //assert
            const string expected = "1 2 lucky 4";
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Should_return_expexted_result_when_range_contains_a_number_that_is_multiples_by_15()
        {
            //arrange
            var numbers = Enumerable.Range(13, 3);
            var evaluator = new Evaluator();

            //arrange
            string actual = evaluator.Evaluate(numbers);

            //assert
            const string expected = "lucky 14 fizzbuzz";
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Should_return_expexted_result_when_range_contains_a_number_that_is_multiples_by_5()
        {
            //arrange
            var numbers = Enumerable.Range(4, 2);
            var evaluator = new Evaluator();

            //arrange
            string actual = evaluator.Evaluate(numbers);

            //assert
            const string expected = "4 buzz";
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Should_return_expexted_result_when_range_contains_a_number_that_is_multiples_by_3_5_15()
        {
            //arrange
            var numbers = Enumerable.Range(1, 20);
            var evaluator = new Evaluator();

            //arrange
            string actual = evaluator.Evaluate(numbers);

            //assert
            const string expected = "1 2 lucky 4 buzz fizz 7 8 fizz buzz 11 fizz lucky 14 fizzbuzz 16 17 fizz 19 buzz";
            Assert.AreEqual(expected, actual);
        }
    }

}
