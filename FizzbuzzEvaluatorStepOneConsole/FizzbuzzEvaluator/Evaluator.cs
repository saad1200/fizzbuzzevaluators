﻿using System.Collections.Generic;
using System.Text;

namespace FizzbuzzEvaluator
{
    public class Evaluator
    {
        public string Evaluate(IEnumerable<int> numbers)
        {
            var result = new StringBuilder();
            foreach (var i in numbers)
            {
                if (i % 15 == 0)
                    result.Append("fizzbuzz");
                else if (i % 5 == 0)
                    result.Append("buzz");
                else if (i % 3 == 0)
                    result.Append("fizz");
                else
                    result.Append(i);

                result.Append(' ');
            }

            return result.ToString().Trim();
        }
    }
}
