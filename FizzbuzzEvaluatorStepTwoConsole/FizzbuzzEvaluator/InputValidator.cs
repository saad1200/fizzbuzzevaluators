﻿using System.Text.RegularExpressions;

namespace FizzbuzzEvaluator
{
    public class InputValidator
    {
        private readonly Regex regex = new Regex("^[\\d]+-[\\d]+$");

        public bool IsValid(string input)
        {
            return !(string.IsNullOrEmpty(input) || !regex.IsMatch(input));
        }
    }
}
