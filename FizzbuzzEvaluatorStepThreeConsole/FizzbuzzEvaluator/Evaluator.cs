﻿using System.Collections.Generic;
using System.Text;

namespace FizzbuzzEvaluator
{
    public class Evaluator
    {
        private readonly Report report;

        public Evaluator(Report report)
        {
            this.report = report;
        }

        public string Evaluate(IEnumerable<int> numbers)
        {
            var result = new StringBuilder();
            foreach (var i in numbers)
            {
                string keyword = i.ToString();
                if (i.ToString().Contains("3"))
                    keyword = "lucky";
                else if (i % 15 == 0)
                    keyword = "fizzbuzz";
                else if (i % 5 == 0)
                    keyword = "buzz";
                else if (i % 3 == 0)
                    keyword = "fizz";

                report.Add(keyword);
                result.Append(keyword).Append(' ');
            }

            return result.ToString().Trim();
        }
    }
}
