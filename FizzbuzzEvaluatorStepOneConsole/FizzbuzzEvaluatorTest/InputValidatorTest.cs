﻿using FizzbuzzEvaluator;
using NUnit.Framework;

namespace FizzbuzzEvaluatorTest
{
    [TestFixture]
    public class InputValidatorTest
    {
        [Test]
        public void Should_be_invalid_when_range_starts_with_chararter()
        {
            //arrange
            const string input = "a-1";
            var evaluate = new InputValidator();

            //action
            bool isValid = evaluate.IsValid(input);

            //assert
            Assert.IsFalse(isValid);

        }

        [Test]
        public void Should_be_invalid_when_range_starts_with_negative_number()
        {
            //arrange
            const string input = "-1-8";
            var evaluate = new InputValidator();

            //action
            bool isValid = evaluate.IsValid(input);

            //assert
            Assert.IsFalse(isValid);
        }

        [Test]
        public void Should_be_invalid_when_range_starts_with_real_number()
        {
            //arrange
            const string input = "3.3-7";
            var validator = new InputValidator();

            //action
            bool isValid = validator.IsValid(input);

            //assert
            Assert.IsFalse(isValid);
        }

        [Test]
        public void Should_be_invalid_when_input_format_is_invalid()
        {
            //arrange
            const string input = "3,7";
            var validator = new InputValidator();

            //action
            bool isValid = validator.IsValid(input);

            //assert
            Assert.IsFalse(isValid);
        }

        [Test]
        public void Should_be_invalid_when_input_is_null()
        {
            //arrange 
            var validator = new InputValidator();

            //action
            bool isValid = validator.IsValid(null);

            //assert
            Assert.IsFalse(isValid);
        }

        [Test]
        public void Should_be_invalid_when_input_is_empty()
        {
            //arrange 
            var validator = new InputValidator();

            //action
            bool isValid = validator.IsValid("");

            //assert
            Assert.IsFalse(isValid);
        }

        [Test]
        public void Should_return_expected_result_when_start_is_greater_than_end_number()
        {
            //arrange
            const string input = "7-1";
            var validator = new InputValidator();

            //action
            bool isValid = validator.IsValid(input);

            //assert
            Assert.IsTrue(isValid);
        }

        [Test]
        public void Should_return_expected_result_when_input_is_valid()
        {
            //arrange 
            const string input = "1-20";
            var validator = new InputValidator();

            //action
            bool isValid = validator.IsValid(input);

            //assert
            Assert.IsTrue(isValid);
        }
    }

}
